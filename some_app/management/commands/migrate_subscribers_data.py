import csv
from datetime import datetime

from django.core.management.base import BaseCommand
from django.db import connection
from django.db.models import Subquery
from django.utils.timezone import utc

from some_app.models import Subscriber, User, Client, SubscriberSMS


class Command(BaseCommand):
    help = "Migrates subscribers' data to user"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.db_proxy = DBProxy()

    def add_arguments(self, parser):
        parser.add_argument('filename', nargs='?', type=str)

    def handle(self, *args, **options):
        db_data = self._fetch_data()
        data_dicts = self._prepare_data(db_data['client_data'], db_data['user_data'])

        self._process_subscriber_data(db_data['subscriber_data'], data_dicts)
        self._process_subscriber_sms_data(db_data['subscriber_sms_data'], data_dicts)
        self.db_proxy.commit_deferred_objects()
        print(len(connection.queries))

    def _fetch_data(self):
        client_data = self.db_proxy.fetch(model=Client)
        user_data = self.db_proxy.fetch(model=User)
        subscriber_data = self.db_proxy.fetch(model=Subscriber, field_name='email', exclude_existing_users=True)
        subscriber_sms_data = self.db_proxy.fetch(model=SubscriberSMS, field_name='phone', exclude_existing_users=True)
        return {
            'client_data': client_data,
            'subscriber_data': subscriber_data,
            'subscriber_sms_data': subscriber_sms_data,
            'user_data': user_data
        }

    def _prepare_data(self, client_data, user_data):
        return {
            'clients_email_dict': self._get_field_to_data_dict('email', user_data),
            'clients_phone_dict': self._get_field_to_data_dict('phone', user_data),
            'users_email_dict': self._get_field_to_data_dict('email', client_data),
            'users_phone_dict': self._get_field_to_data_dict('phone', client_data)
        }

    def _get_field_to_data_dict(self, field_name, data):
        return {getattr(x, field_name): x for x in data}

    # TODO: unify two following methods by extracting common code
    def _process_subscriber_data(self, subscriber_data, data):
        subscriber_conflicts = []
        for subscriber in subscriber_data:
            if client := data['clients_email_dict'].get(subscriber.email):
                if not (client.phone in data['users_phone_dict'] and client.email not in data['users_email_dict']):
                    self.db_proxy.save_user(client.email, client.phone, subscriber.gdpr_consent)
                else:
                    subscriber_conflicts.append((subscriber.id, subscriber.email))
            else:
                self.db_proxy.save_user(email=subscriber.email, gdpr_consent=subscriber.gdpr_consent)
        if subscriber_conflicts:
            self._log_conflicts(subscriber_conflicts, 'subscriber_conflicts')

    def _process_subscriber_sms_data(self, subscriber_data, data):
        subscriber_sms_conflicts = []
        for subscriber in subscriber_data:
            if client := data['clients_phone_dict'].get(subscriber.phone):
                if not (client.email in data['users_email_dict'] and client.phone not in data['users_phone_dict']):
                    self.db_proxy.save_user(client.email, client.phone, subscriber.gdpr_consent)
                else:
                    subscriber_sms_conflicts.append((subscriber.id, subscriber.phone))
            else:
                self.db_proxy.save_user(phone=subscriber.phone, gdpr_consent=subscriber.gdpr_consent)
        if subscriber_sms_conflicts:
            self._log_conflicts(subscriber_sms_conflicts, 'subscriber_sms_conflicts')

    def _log_conflicts(self, data, filename):
        with open(filename, 'w') as f:
            writer = csv.writer(f)
            writer.writerows(data)


class DBProxy:
    def __init__(self):
        self.deferred_save_objects = []

    def fetch(self, *args, **kwargs):
        model = kwargs.get('model')
        qs = model.objects.all()
        # fetch only subscribers that given field name values aren't yet in User table
        if kwargs.get('exclude_existing_users'):
            field_name = kwargs.get('field_name')
            qs_expression = {
                f'{field_name}__in': Subquery(User.objects.filter(
                    **{f'{field_name}__isnull': False}
                ).values(field_name).values(field_name))
            }
            qs = qs.exclude(**qs_expression)
        return qs

    def save_user(self, email=None, phone=None, gdpr_consent=None):
        user = User(
            create_date=datetime.utcnow().replace(tzinfo=utc), email=email,
            phone=phone, gdpr_consent=gdpr_consent
        )
        self.deferred_save_objects.append(user)

    def commit_deferred_objects(self):
        User.objects.bulk_create(self.deferred_save_objects)

