from django.db import models


class Subscriber(models.Model):
    id = models.IntegerField(primary_key=True)
    create_date = models.DateTimeField()
    email = models.TextField(unique=True)
    gdpr_consent = models.BooleanField()


class SubscriberSMS(models.Model):
    id = models.IntegerField(primary_key=True)
    create_date = models.DateTimeField()
    phone = models.TextField(unique=True)
    gdpr_consent = models.BooleanField()


class Client(models.Model):
    id = models.IntegerField(primary_key=True)
    create_date = models.DateTimeField()
    email = models.TextField(unique=True)
    phone = models.TextField()


class User(models.Model):
    id = models.IntegerField(primary_key=True)
    create_date = models.DateTimeField()
    email = models.TextField(null=True)
    phone = models.TextField(null=True)
    gdpr_consent = models.BooleanField()
